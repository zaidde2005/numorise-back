FROM php:7.1.2-apache
COPY . /user/src/numorise/
COPY ./docker/numorise.conf /etc/apache2/sites-enabled/
COPY ./docker/.env.test /user/src/numorise/.env
WORKDIR /user/src/numorise
RUN a2enmod rewrite
RUN rm /etc/apache2/sites-enabled/000-default.conf
RUN service apache2 restart
