<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEntryVleuAddUnit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('entry_values', function ($table) {
            $table->string('unit')->nullable();
            $table->unique(['date', 'entry_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entry_values', function ($table) {
           $table->dropColumn('unit');
           $table->dropUnique(['date', 'entry_id']);
        });
    }
}
