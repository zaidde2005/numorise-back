<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('pages', function ($table) {
            $table->integer('attachment_id')
                ->unsigned()->nullable();
            $table->foreign('attachment_id')
                ->references('id')
                ->on('attachments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function ($table) {
           $table->dropForeign(['attachment_id']);
           $table->dropColumn('attachment_id');
        });
    }
}
