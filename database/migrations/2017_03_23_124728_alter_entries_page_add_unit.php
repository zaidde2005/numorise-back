<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEntriesPageAddUnit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE entry_values ALTER COLUMN value TYPE numeric(21, 5) USING 5.5');
        Schema::table('entry_values', function ( $table ) {
            $table->boolean('is_percentage')->default(FALSE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entry_values', function ( $table ) {
            $table->dropColumn('is_percentage');
        });
    }
}
