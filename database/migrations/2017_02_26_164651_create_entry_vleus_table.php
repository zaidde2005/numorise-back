
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntryVleusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entry_values',function($table){
          $table->increments('id');
          $table->string('value');
          $table->string('description')->nullable();
          $table->string('source');
          $table->date('date');
          $table->integer('entry_id')->unsigned();
          $table->foreign('entry_id')
            ->references('id')
            ->on('entries')
            ->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('entry_values');
    }
}
