<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEntryValuesAddIsPerc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SChema::table('entry_values', function ( $table ) {

            $table->boolean('is_percentage')->default(False);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        SChema::table('entry_values', function ( $table ) {
            $table->dropColumn('is_percentage');
        });
    }
}
