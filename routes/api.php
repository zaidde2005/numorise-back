<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group(["prefix" => "v1/"],function(){
    Route::get("search/{key}", "SearchController@show");
    Route::post("register", 'Auth\RegisterController@register');
    Route::post("login ", 'Auth\LoginController@login');
    Route::get('page/{id}', 'PageController@show');
    Route::get('page/{id}/photo', 'PagePhotoController@show');
    Route::get('attachment/{di}', 'AttachmentController@show');
    Route::get('entry/{entry}', 'EntryController@show');
});

//Auth::routes(["prefix" => "v1/"]);

Route::group(["prefix"=>"v1/", "middleware" => "auth:api"],function () {
    Route::resource("page", "PageController", ["except" => ["edit", "create", "show"]]);
    Route::resource("page.entry", "EntryController", ["only" => ["index", "store"]]);
    Route::put("entry/{entry}","EntryController@update");
    Route::resource("entry.value", "ValueController", ["only" => ["index", "store"]]);
    Route::put("value/{value}","ValueController@update");
    Route::post('attachment', 'AttachmentController@store');
    Route::resource('follow', 'FollowController', ['only' => ['store', 'destroy', 'index']]);
    Route::get('trending', 'TrendingController@index');
});
