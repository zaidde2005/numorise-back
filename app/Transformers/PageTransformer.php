<?php

namespace App\Transformers;

use App\Page;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: zaid
 * Date: 4/2/17
 * Time: 6:26 PM
 */
class PageTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'entries'
    ];

    public function transform(Page $page)
    {
        return [
            'id'          => (int)$page->id,
            'title'       => $page->title,
            'description' => $page->description,
            'website'     => $page->website,
            'tags'        => $page->tags,
            //@todo move to save
            'photo'       => action('PagePhotoController@show', ['id' => $page->id]),
            'verified'    => $page->verified
        ];
    }

    public function includeEntries(Page $page)
    {
        $entries = $page->entries;
        return $this->collection($entries, new EntryTransformer);
    }
}