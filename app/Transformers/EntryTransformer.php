<?php

namespace App\Transformers;

use App\Entry;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: zaid
 * Date: 4/3/17
 * Time: 8:58 AM
 */
class EntryTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'values', 'page'
    ];


    public function transform(Entry $entry)
    {
        return [
            'id'    => (int)$entry->id,
            'title' => $entry->title
        ];
    }

    public function includeValues  ( Entry $entry ) {
        $values = $entry->values;
        return $this->collection($values, new ValueTransformer);
    }

    public function includePage( Entry $entry ){
        $page = $entry->page;
        return $this->item($page, new PageTransformer);
    }
}