<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class CreatedByTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'name' => $user->name
        ];
    }
}
