<?php
/**
 * Created by PhpStorm.
 * User: zaid
 * Date: 4/3/17
 * Time: 9:06 AM
 */

namespace App\Transformers;


use App\Value;
use League\Fractal\TransformerAbstract;

class ValueTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'createdBy'
    ];

    public function transform(Value $value)
    {
        return [
            'value'       => (double)$value->value,
            'description' => $value->description,
            'source'      => $value->source,
            'date'        => $value->date,
            'created_at'  => $value->created_at->format('M d,Y'),
            'unit'        => $value->unit
        ];
    }

    public function includeCreatedBy(Value $value)
    {
        $createdBy = $value->createdBy;
        return $this->item($createdBy, new CreatedByTransformer);
    }
}