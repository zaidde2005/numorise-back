<?php

namespace App\Observers;

use App\NModle;

class NObserver
{
    public function creating(NModle $modle){
        $modle->created_by = \Auth::id();
        $modle->updated_by = \Auth::id();
    }

    public function updating(NModle $modle){
        $modle->updated_by = \Auth::id();
    }
}