<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Page extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              'title'       => 'string|required',
              'value'       => 'numeric|required',
              'description' => 'string',
              'source'      => 'string|required',
              'date'        => 'date|required',
        ];
    }

    public function wantsJson()
    {
        return true;
    }
}
