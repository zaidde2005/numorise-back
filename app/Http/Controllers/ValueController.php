<?php

namespace App\Http\Controllers;

use App\Entry;
use App\Value;
use illuminate\Http\Request;

class ValueController extends Controller
{

    public function store(Request $req, $entry_id)
    {
        $entry = Entry::findOrFail($entry_id);

        $this->validate($req, [
            'value'         => 'numeric|required',
            'description'   => 'string',
            'source'        => 'string|required',
            'date'          => ['string', 'required'],
            'is_percentage' => 'required|boolean'
        ]);


        return $entry->values()->create($req->all());
    }

    public function update($id, Request $req)
    {
        $value = Value::findOrFail($id);

        $this->validate($req, [
            'value'         => 'numeric|required',
            'description'   => 'string',
            'source'        => 'string|required',
            'date'          => 'string|required',
            'is_percentage' => 'required|boolean'
        ]);


        $value->fill($req->all())->save();
        return $value;
    }

    public function index($entry_id)
    {
        $entry = Entry::findOrFail($entry_id);

        return $entry->values;
    }
}
