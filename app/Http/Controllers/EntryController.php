<?php

namespace App\Http\Controllers;

use App\Entry;
use App\Http\Requests\PageRequest;
use App\Transformers\EntryTransformer;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\QueryException;
use illuminate\Http\Request;
use App\Page;
use Mockery\Exception;
use Spatie\Fractalistic\ArraySerializer;

class EntryController extends Controller
{

    public function store(Page $page,PageRequest $req)
    {
        try{
            \DB::beginTransaction();
            $entry = $page->entries()->create($req->all());
            $entry->values()->create($req->all());
            $entry->load('values');
            $page->searchable();
            \DB::commit();
        }
        catch(Exception $exception){
            \DB::rollback();
            response()->setStatusCode(500);
        }
        catch (ClientException $exception){
            \DB::rollback();
            return $exception->getResponse()->getBody()->getContents();
        }

        return fractal()::create()
                    ->parseIncludes('values.createdBy')
                    ->item($entry,new EntryTransformer())
                    ->serializeWith(new ArraySerializer())
                    ->toArray();
    }

    public function update($id, Request $req)
    {
        $entry = Entry::findOrFail($id);


        $this->validate($req, [
            'title'  => "string|required",
        ]);


        $entry->fill($req->all())->save();
        return $entry;
    }

    public function index($id)
    {
        $page = Page::findOrFail($id);
        return $page->entries;
    }

    public function show(Entry $entry){
        return fractal()::create()
            ->parseIncludes(['values.createdBy', 'page'])
            ->item($entry, new EntryTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
    }
}
