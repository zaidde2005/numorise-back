<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Page;
use illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Transformers\PageTransformer;
use Spatie\Fractalistic\ArraySerializer;

class PageController extends Controller
{

    public function store(Request $req)
    {

        $this->validate($req, [
            'title'          => 'string|required|unique:pages',
            'description'   => 'string|required',
            'tags'          => 'string|required',
            'attachment_id' => 'exists:attachment_user,attachment_id|unique:pages'
        ]);
//    todo add transaction
        $page = new Page($req->all());

        if(!$req->get('attachment_id'))
        {
            $file = uniqid('default');
            $atach = new Attachment(['file'=>$file . ".jpg"]);
            $atach->save();
            $av = \Avatar::create($req->get('title'))->toBase64();
            $av->save(config('systemvars.up_path') . "/$atach->file");
            $page->attachment()->associate($atach);

        }
        $page->save();
        $page->load(['attachment']);
        return fractal()::create()
            ->parseIncludes('entries.values.createdBy')
            ->item($page, new PageTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
    }

    public function update($id, Request $req)
    {
        $page = Page::find($id);
        if (!$page)
            abort(404);
        $this->validate($req, [
            'title'         => ['string', 'required', Rule::unique('pages')->ignore($page->id)],
            'description'   => 'string|required',
            'tags'          => 'string|required',
            'attachment_id' => 'exists:attachments,id|unique:pages,id,' . $page->id . '|exists:attachments,id'
        ]);

        $page->fill($req->all())->save();
        if($att = $req->get('attachment_id'))
            Attachment::find($att)->user()->detach();
        return fractal()::create()
            ->parseIncludes('entries.values.createdBy')
            ->item($page, new PageTransformer())
            ->serializeWith(new ArraySerialize.r())
            ->toArray();
    }

    public function index()
    {
        return Page::all();
    }

    public function destroy($id)
    {
        return Page::findOrFail($id);
    }

    public function show($id)
    {
        $page = Page::with(['entries.values.createdBy'])->findOrFail($id);
        $page->view()->create([]);
        return fractal()::create()
            ->parseIncludes('entries.values.createdBy')
            ->item($page, new PageTransformer())
            ->serializeWith(new ArraySerializer())
            ->toArray();
    }
}
