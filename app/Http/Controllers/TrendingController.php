<?php

namespace App\Http\Controllers;

use App\Page;
use App\View;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrendingController extends Controller
{
    public function index(){
        $from = new Carbon('first day of this year');
        return View::with(['page'])
            ->select('page_id')
            ->groupBy('page_id')
            ->orderBy(\DB::raw('count(page_id)'))
            ->where('created_at', '>', $from)
            ->get()->pluck('page');
    }
}
