<?php

namespace App\Http\Controllers;

use App\Page;
use App\User;
use Illuminate\Http\Request;

class FollowController extends Controller
{
    public function store(Request $request){
        $page = Page::findOrFail($request->get('page_id'));
        $page->followers()->attach(\Auth::id());
        return ['message' => 'success'];
    }

    public function destroy($id){

        $page = Page::whereHas('followers', function ( $query ) {
            $query->where('id', \Auth::id());
        })->findOrFail($id);
        $page->followers()->detach(\Auth::id());
        return ['message' => 'success'];
    }

    public function index(){

        $user = User::findOrFail(\Auth::id());
        return $user->follows;

    }
}
