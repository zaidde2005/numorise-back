<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attachment;

class AttachmentController extends Controller
{
    public function store(Request $request){

        $this->validate($request,[
            'file' => 'required|mimes:' . config('systemvars.accepted_files')
        ]);
        $att = new Attachment($request->all());
        $att->upload($request->file('file'));
        $att->save();
        $att->user()->attach(\Auth::id(),['created_at' => new \DateTime()]);
        return [
            'url' => action('AttachmentController@show', ['id' => $att->id]),
            'id'  => $att->id
        ];
    }

    public function show($id){
        return Attachment::find($id)->download();
    }
}
