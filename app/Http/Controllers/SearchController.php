<?php
namespace App\Http\Controllers;

use App\Page;

 class SearchController extends Controller{

   public function show($key) {
     return Page::where('name','like',"%$key%")
      ->with(['entries.values' ])
      ->get();
   }
 }
