<?php

namespace App\Http\Controllers;

use App\Attachment;
use Illuminate\Http\Request;

class PagePhotoController extends Controller
{
    public function show($id)
    {

        $attach = Attachment::whereHas('page', function ( $query ) use($id) {
            $query->where('id', $id);
        })->first() or abort(404);
        return $attach->download();
    }
}
