<?php
namespace App;

use App\NTarits\Observable;
use App\Transformers\PageTransformer;
use Laravel\Scout\Searchable;
use Spatie\Fractalistic\ArraySerializer;


class Page extends NModle{
  use Searchable,Observable;

  protected $fillable = ['tags', 'description', 'title', 'attachment_id'];

  protected $hidden = ['views'];

  public function entries(){
    return $this->hasMany('App\Entry');
  }

  public function toSearchableArray(){
      return fractal()::create()
          ->parseIncludes('entries.values.createdBy')
          ->item($this, new PageTransformer())
          ->serializeWith(new ArraySerializer())
          ->serializeWith(new ArraySerializer())
          ->toArray();
  }

  public function attachment(){
      return $this->belongsTo('\App\Attachment');
  }

  public function view(){
      return $this->hasMany('\App\View');
  }

  public function followers(){
      return $this->belongsToMany('\App\User');
  }

}


