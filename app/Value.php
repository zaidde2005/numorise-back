<?php

namespace App;

use App\NTarits\Observable;
use GuzzleHttp\Client;
use illuminate\Database\Eloquent\Model;

class Value extends NModle
{

    use Observable;

    protected $fillable = ['value', 'description', 'source', 'date', 'unit'];
    protected $hidden = ['entry_id'];
    protected $table = 'entry_values';

    public function entry()
    {
        return $this->belongsTo('Entry');
    }


    public function createdBy(){
        return $this->belongsTo('\App\User','created_by');
    }

    public function toSearchableArray(){
        $temp = $this->toArray();
        $temp['createdBy'] = $this->createdBy->name;
    }

    public function setSourceAttribute($value){
        if(!filter_var($value, FILTER_VALIDATE_URL))
            return $this->attributes['source'] = $value;
        $client = new Client();
        $url = 'https://www.googleapis.com/urlshortener/v1/url';
//        @todo move configuration to .env
        $res = $client->post($url, [
            'json'        => ['longUrl' => $value],
            'SYNCHRONOUS' => true,
            'query'       => ['key' => 'AIzaSyDIGZ3oxTa1h9wXBJVoXo3Lty6N-F76Wzo'],
        ])->getBody();
        $res = json_decode($res);
        return $this->attributes['source'] = $res->id;
    }
}
