<?php

namespace  App\NTarits;

use App\Observers\NObserver;

trait Observable
{

    public static function bootObservable(){

        self::observe(NObserver::class);
    }
}