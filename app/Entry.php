<?php

namespace App;





use App\NTarits\Observable;

class Entry extends NModle
{

    use Observable;

    protected $fillable = ['title'];
    protected $hidden = ['page_id'];

    public function values()
    {
        return $this->hasMany('App\Value');
    }

    public function page(){
        return $this->belongsTo('App\Page');
    }
}
