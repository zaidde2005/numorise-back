<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
class Attachment extends Model
{

    protected $fillable = ['name', 'file'];
    /**
     * overridden to generate random unique name for the uploaded attachment on fill
     * @param array $ttributes
     * @return $this
     */
    public function fill(array $attributes)
    {


        if (!empty($attributes['file']) && is_uploaded_file($attributes['file'])) {
            $file = $attributes['file'];
            $attributes['file'] = uniqid('numorise') . '.'
                . $file->getClientOriginalExtension();

            $attributes['name'] = $file->getClientOriginalName();
            return parent::fill($attributes);
        }

        $attributes['name'] = 'default';
        return parent::fill($attributes);
    }

    /**
     * upload files to directory specified on the up_path
     * @param $file
     */
    public function upload($file)
    {
        return $file->move(config('systemvars.up_path'), $this->file);
    }

    public function user()
    {
        return $this->belongsToMany('\App\User');
    }

    public function page()
    {
        return $this->hasOne('\App\Page');
    }

    public function download(){
        if(!File::exists($this->getPath()))
            abort(404);
        return response()->download($this->getPath());
    }

    public function getPath(){
        return config('systemvars.up_path'). '/' . $this->file;
    }
}
