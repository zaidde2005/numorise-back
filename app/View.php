<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{

    protected $fillable = ['client_ip', 'user_id'];
    public function fill(Array $attributes){

        if(\Auth::id())
            $attributes['user_id'] = \Auth::id();

        $attributes['client_ip'] = \Request::getClientIp(true);
//        $attributes['location'] = geoip($attributes['client_ip']);

        return parent::fill($attributes);

    }

    public function page(){
        return $this->belongsTo('\App\Page');
    }
}
